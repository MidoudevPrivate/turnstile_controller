from time import sleep

import serial.tools.list_ports
from fastapi import FastAPI, HTTPException

app = FastAPI()
arduino = serial.Serial()

ports = serial.tools.list_ports.comports()


def setup_arduino():
    arduino.port = "COM3"
    arduino.baudrate = 9600
    arduino.open()


def reset_arduino():
    arduino.port = "COM3"


try:
    setup_arduino()
except Exception as e:
    print("Arduino not found")


@app.get("/")
async def root():
    # test the Arduino

    return {"message": "Welcome to the door opener!, go to /door/open to open the door or /door/close to close it"}


# door availibility check
@app.get("/door/available")
async def door_available():
    try:
        arduino.write(b'5')
        return {"message": "Door is available"}
    except Exception as e:
        reset_arduino()
        # raise fastapi.HTTPException(status_code=500, detail=str(e))
        raise HTTPException(status_code=500, detail=str(e))


@app.options("/door/open")
async def door_open_options():
    try:
        # test the arduino
        arduino.write(b'1')
        sleep(2)
        arduino.write(b'0')
    except Exception as e:
        print(e)
        reset_arduino()
        # test the Arduino
        arduino.write(b'1')
        sleep(2)
        arduino.write(b'0')
    return {"message": "Opening door"}
